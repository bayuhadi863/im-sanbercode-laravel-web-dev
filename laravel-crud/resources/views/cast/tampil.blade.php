@extends('layouts.master')

@section('title')
Halaman Tampil Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm my-3">Tambah Cast</a>

<table class="table">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Bio</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($cast as $key => $item)
      <tr>
        <th scope="row">{{$key + 1}}</th>
        <td>{{$item->nama}}</td>
        <td>{{$item->umur}}</td>
        <td>{{$item->bio}}</td>
        <td>
          <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm m-1">Detail</a>
          <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm m-1">Edit</a>
          <form action="/cast/{{$item->id}}" method="post">
            @csrf
            @method('delete')
            <input type="submit" class="btn btn-danger btn-sm m-1" value="Delete">
          </form>
        </td>
      </tr>
    @empty
      <h2>Data Cast Kosong</h2>
    @endforelse
  </tbody>
</table>

@endsection