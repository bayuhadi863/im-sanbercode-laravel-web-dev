@extends('layouts.master')

@section('title')
Halaman Tambah Cast
@endsection

@section('content')
<form action="/cast" method="post">
  @csrf
  <div class="form-group">
    <label for="name">Nama</label>
    <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" id="nama" >
  </div>
    @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <div class="form-group">
    <label for="umur">Umur</label>
    <input type="number" name="umur" class="form-control @error('umur') is-invalid @enderror" id="umur" >
  </div>
    @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <div class="form-group">
    <label for="bio">Bio</label>
    <textarea name="bio" id="bio" class="form-control @error('bio') is-invalid @enderror" cols="30" rows="5"></textarea>
  </div>
    @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection