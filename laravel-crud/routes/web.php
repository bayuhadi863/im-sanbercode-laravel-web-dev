<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);

Route::get('/register', [AuthController::class, 'register']);

Route::post('/welcome', [AuthController::class, 'welcome']);

Route::get('/data-tables', function(){
  return view('page.data-tables');
});

Route::get('/table', function(){
  return view('page.table');
});

// CRUD

// ruote mengarah ke form 
Route::get('/cast/create', [CastController::class, 'create']);

//Route untuk insert data inputan ke database table cast
Route::post('/cast', [CastController::class, 'store']);

//Route untuk menampilkan semua data pada tabel cast
Route::get('/cast', [CastController::class, 'index']);
//Route untuk menampilkan detail tabel cast

Route::get('/cast/{cast_id}', [CastController::class, 'show']);
//Route untuk mengarah ke form edit data dengan params id
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
//Route update data
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

//Route untuk delete data
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);

