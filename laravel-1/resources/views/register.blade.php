<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Form</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
      @csrf
      <label for="firstName">First name:</label><br /><br />
      <input type="text" id="firstName" name="firstName" /><br /><br />
      <label for="lastName">Last name:</label><br /><br />
      <input type="text" id="lastName" name="lastName" /><br /><br />
      <label>Gender:</label><br /><br />
      <input type="radio" id="male" name="gender" value="male" />
      <label for="male">Male</label><br />
      <input type="radio" id="female" name="gender" value="female" />
      <label for="female">Female</label><br />
      <input type="radio" id="other" name="gender" value="other" />
      <label for="other">other</label><br /><br />
      <label for="nationality">Nationality:</label><br /><br />
      <select id="nationality" name="nationality">
        <option value="indonesian" selected>Indonesia</option>
        <option value="singaporean">Singaporean</option>
        <option value="malaysian">Malaysian</option>
        <option value="australian">Australian</option></select
      ><br /><br />
      <label>Language Spoken:</label><br /><br />
      <input
        type="checkbox"
        name="language"
        value="bahasaIndonesia"
        id="bahasaIndonesia"
      />
      <label for="bahasaIndonesia">Bahasa Indonesia</label><br />
      <input type="checkbox" name="language" value="english" id="english" />
      <label for="english">English</label><br />
      <input type="checkbox" name="language" value="other" id="other" />
      <label for="other">Other</label><br /><br />
      <label for="bio">Bio:</label><br /><br />
      <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br />
      <input type="submit" value="Sign Up" name="submit" />
    </form>
  </body>
</html>