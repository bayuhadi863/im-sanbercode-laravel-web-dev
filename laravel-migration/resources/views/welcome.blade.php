@extends('layouts.master')

@section('title')
Halaman Welcome
@endsection

@section('content')
<h1>Selamat Datang {{$firstName}} {{$lastName}}</h1>
<h2>
  Terima kasih telah begabung di Sanberbook. Social Media kita bersama!
</h2>
@endsection