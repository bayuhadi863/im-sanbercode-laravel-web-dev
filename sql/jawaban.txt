1. MEMBUAT DATABASE

    CREATE DATABASE myshop;

2. MEMBUAT TABEL

  a. Tabel users

      CREATE TABLE users(
        id INT NOT NULL AUTO_INCREMENT,
        name VARCHAR(255) NOT NULL,
        email VARCHAR(255) NOT NULL,
        password VARCHAR(255) NOT NULL,
        PRIMARY KEY (id)
      )ENGINE=innodb;

  b. Tabel categories

      CREATE TABLE categories(
        id INT NOT NULL AUTO_INCREMENT, 
        name VARCHAR(255) NOT NULL, 
        PRIMARY KEY (id)
      )ENGINE=innodb;

  c. Tabel items

      CREATE TABLE items (
        id INT NOT NULL AUTO_INCREMENT,
        name VARCHAR(255) NOT NULL,
        description VARCHAR(255) NOT NULL,
        price INT NOT NULL,
        stock INT NOT NULL,
        category_id INT NOT NULL,
        PRIMARY KEY (id),
        FOREIGN KEY (category_id) REFERENCES categories(id)
      )ENGINE=innodb;

3. MEMASUKKAN DATA PADA TABEL

  a. Table users

      INSERT INTO users (name, email, password) 
      VALUES 
      ("John Doe", "john@doe.com", "john123"), 
      ("Jane Doe", "jane@doe.com", "jenita123");

  b. Tabel categories

      INSERT INTO categories (name) 
      VALUES 
      ("gadget"), 
      ("cloth"), 
      ("men"), 
      ("women"), 
      ("branded");

  c. Tabel items

      INSERT INTO items (name, description, price, stock, category_id) 
      VALUES 
      ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1), 
      ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2), 
      ("IMWHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);

4. MENGAMBIL DATA DARI DATABASE

  a. Mengambil Data users

      SELECT id, name, email FROM users;

  b. Mengambil data items JOIN categories

      SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori 
      FROM items 
      JOIN categories ON items.category_id = categories.id;

5. MENGUBAH DATA 

    UPDATE items 
    SET price = 2500000 
    WHERE id = 1;
